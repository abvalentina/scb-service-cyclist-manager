package com.bsi.scbservicecyclistmanager.domain;

import java.util.UUID;

public class Cyclist {
    private UUID id;
    private Email email;
    private String name;
    private String password;
    private Boolean BRIndicator;
    private Boolean usingBike;
    private Document document;
    private Bicycle bike;
    private CreditCard creditCard;

    public Cyclist(){}

    public Cyclist(UUID idCyclist, Email email, String name, String password, Boolean BRIndicator, Boolean usingBike, Document document, Bicycle bike, CreditCard creditCard){
        this.id = idCyclist;
        this.email = email;
        this.name = name;
        this.password = password;
        this.BRIndicator = BRIndicator;
        this.usingBike = usingBike;
        this.document = document;
        this.bike = bike;
        this.creditCard = creditCard;
    }

    public Email getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public Boolean getUsingBike() {
        return usingBike;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getBRIndicator() {
        return BRIndicator;
    }

    public void setBRIndicator(Boolean BRIndicator) {
        this.BRIndicator = BRIndicator;
    }

    public void setUsingBike(Boolean usingBike) {
        this.usingBike = usingBike;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public Bicycle getBike() {
        return bike;
    }

    public void setBike(Bicycle bike) {
        this.bike = bike;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }
}

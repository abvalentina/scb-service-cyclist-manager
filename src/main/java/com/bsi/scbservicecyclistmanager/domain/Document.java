package com.bsi.scbservicecyclistmanager.domain;

import java.awt.*;

public class Document {

    private String type;
    private Number number;
    private Image photo;

    public Document(){

    }
    public Document(String type, Number number, Image photoDocument){
        this.type = type;
        this.number = number;
        this.photo = photoDocument;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Number getNumber() {
        return number;
    }

    public void setNumber(Number number) {
        this.number = number;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }
}

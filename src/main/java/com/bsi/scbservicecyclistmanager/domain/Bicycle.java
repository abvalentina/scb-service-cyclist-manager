package com.bsi.scbservicecyclistmanager.domain;

import java.util.UUID;

public class Bicycle {
    private Integer id;
    private Integer code;
    private String status;
    private String brand;
    private String model;
    private Integer year;
    private String localization;


    public Bicycle(Integer idBicycle, Integer codeBicycle, String status){
        this.id = idBicycle;
        this.code = codeBicycle;
        this.status = status;
    }

    public Bicycle() {   }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

}

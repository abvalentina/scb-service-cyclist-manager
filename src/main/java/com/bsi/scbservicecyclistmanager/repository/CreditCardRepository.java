package com.bsi.scbservicecyclistmanager.repository;

import com.bsi.scbservicecyclistmanager.domain.CreditCard;
import com.bsi.scbservicecyclistmanager.domain.Cyclist;
import com.bsi.scbservicecyclistmanager.domain.Email;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class CreditCardRepository {

    CyclistRepository cyclistRepository;

    protected static ArrayList<CreditCard> allCreditCards;

    private CreditCardRepository(){
        allCreditCards = new ArrayList<>();
        allCreditCards.add(new CreditCard("789966985","", new Date(), 123));
    }

    private static CreditCardRepository instance;

    public static CreditCardRepository instance() {
        if (instance == null) {
            instance = new CreditCardRepository();
        }
        return instance;
    }

    public void addCreditCardList(CreditCard creditCard){
        allCreditCards.add(creditCard);
    }

    public ArrayList<CreditCard> getCreditCardCyclist() {
        return allCreditCards;
    }

    public CreditCard getCreditCardCyclist(UUID idCyclist) throws CyclistNotFoundException {
        try {
            Cyclist cyclist = cyclistRepository.getCyclist(idCyclist);
            if (cyclist.getId().equals(idCyclist))
                if (cyclist.getCreditCard() != null)
                    return cyclist.getCreditCard();
                else
                    throw new CyclistNotFoundException("Não foi possível encontrar o cartão de crédito do ciclista de id " + idCyclist);
        } catch (Exception e) {
            throw new CyclistNotFoundException("Não foi possível encontrar o ciclista de id " + idCyclist);
        }
        return null;
    }

    public CreditCard getCreditCardCyclist(Email emailCyclist) throws CyclistNotFoundException {
        try {
            Cyclist cyclist = cyclistRepository.getCyclist(emailCyclist);
            if (cyclist.getEmail().equals(emailCyclist))
                if (cyclist.getCreditCard() != null)
                    return cyclist.getCreditCard();
                else
                    throw new CyclistNotFoundException("Não foi possível encontrar o cartão de crédito do ciclista de email " + emailCyclist);
        }catch (Exception e){
            throw new CyclistNotFoundException("Não foi possível encontrar o ciclista de email " + emailCyclist);
        }
        return null;
    }

    public CreditCard getCreditCardCyclist(String creditCardNumber) throws CyclistNotFoundException {
        List<Cyclist> cyclists = cyclistRepository.getCyclist();

        for (Cyclist cyclist : cyclists) {
            if (cyclist.getCreditCard().getNumber().equals(creditCardNumber))
                if (cyclist.getCreditCard() != null)
                    return cyclist.getCreditCard();
        }
        throw new CyclistNotFoundException("Não foi possível encontrar o cartão de crédito de numero " + creditCardNumber);
    }
}

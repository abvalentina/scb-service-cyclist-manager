package com.bsi.scbservicecyclistmanager.repository;

import com.bsi.scbservicecyclistmanager.domain.*;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;
import com.bsi.scbservicecyclistmanager.exceptions.UnableToDeleteException;
import com.bsi.scbservicecyclistmanager.exceptions.UnableToInsertException;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class CyclistRepository {

    EmailRepository emailRepository;
    CreditCardRepository creditCardRepository;

    protected static ArrayList<Cyclist> repositoryCyclists;

    private CyclistRepository(){
       repositoryCyclists = new ArrayList<>();
       repositoryCyclists.add(new Cyclist(UUID.fromString("6660bb20-db6f-486d-87aa-183935523d8c"), new Email("fulaninho@email.com"), "", "", true, false, new Document("", 12356, null), new Bicycle(1, 4569854, "INDISPONÍVEL"), new CreditCard("789966985","", new Date(), 123)));
    }

    private static CyclistRepository instance;

    public static CyclistRepository instance() {
        if (instance == null) {
            instance = new CyclistRepository();
        }
        return instance;
    }

    public void postCyclist(Cyclist cyclist) throws UnableToInsertException {
        try{
            emailRepository.addEmailList(cyclist.getEmail());
            creditCardRepository.addCreditCardList(cyclist.getCreditCard());
            repositoryCyclists.add(cyclist);
        }catch (Exception e){
            throw new UnableToInsertException("Não foi possível deletar o ciclista " + cyclist);
        }
    }

    public void deleteCyclist(UUID idCyclist) throws UnableToDeleteException, CyclistNotFoundException {
        Cyclist cyclist = getCyclist(idCyclist);

        try {
            repositoryCyclists.remove(cyclist);
        }catch (Exception e){
            throw new UnableToDeleteException("Não foi possível deletar o ciclista " + cyclist);
        }
    }

    public ArrayList<Cyclist> getCyclist(){
        return repositoryCyclists;
    }

    public Cyclist getCyclist(String nameCyclist) throws CyclistNotFoundException {
        for (Cyclist cyclist : repositoryCyclists) {
            if(cyclist.getName().equals(nameCyclist))
                return cyclist;
        }
        throw new CyclistNotFoundException("Não foi encontrado o ciclista de nome " + nameCyclist);
    }

    public Cyclist getCyclist(UUID idCyclist) throws CyclistNotFoundException{
        for (Cyclist cyclist : repositoryCyclists) {
            if(cyclist.getId().equals(idCyclist))
                return cyclist;
        }
        throw new CyclistNotFoundException("Não foi encontrado o ciclista de id " + idCyclist);
    }

    public Cyclist getCyclist(Email emailCyclist) throws CyclistNotFoundException{
        for (Cyclist cyclist : repositoryCyclists) {
            if(cyclist.getEmail().equals(emailCyclist))
                return cyclist;
        }
        throw new CyclistNotFoundException("Não foi encontrado o ciclista de email " + emailCyclist);
    }

    public Boolean isCyclistThere(UUID id) throws CyclistNotFoundException {
        return getCyclist(id) != null;
    }
}

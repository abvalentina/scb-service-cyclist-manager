package com.bsi.scbservicecyclistmanager.repository;

import com.bsi.scbservicecyclistmanager.domain.Cyclist;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;

import java.util.UUID;

public class BicycleRepository {

    CyclistRepository cyclistRepository;

    public boolean rentBike(UUID idCyclist) throws CyclistNotFoundException {
        Cyclist cyclist = cyclistRepository.getCyclist(idCyclist);
        cyclist.setUsingBike(true);

        throw new CyclistNotFoundException("Não foi possível encontrar o ciclista de id " + idCyclist);
    }
}

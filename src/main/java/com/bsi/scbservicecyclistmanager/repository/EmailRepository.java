package com.bsi.scbservicecyclistmanager.repository;

import com.bsi.scbservicecyclistmanager.domain.Cyclist;
import com.bsi.scbservicecyclistmanager.domain.Email;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;

import java.util.ArrayList;
import java.util.UUID;

public class EmailRepository {

    CyclistRepository cyclistRepository;

    protected static ArrayList<Email> allEmails;

    private EmailRepository() {
        allEmails = new ArrayList<>();
        allEmails.add(new Email("fulaninho@email.com"));
    }

    private static EmailRepository instance;

    public static EmailRepository instance() {
        if (instance == null) {
            instance = new EmailRepository();
        }
        return instance;
    }

    public void addEmailList(Email email) {
        allEmails.add(email);
    }

    public ArrayList<Email> getEmailCyclist() {
        return allEmails;
    }

    public Email getEmailCyclist(String nameCyclist) throws CyclistNotFoundException {
        try {
            Cyclist cyclist = cyclistRepository.getCyclist(nameCyclist);

            return cyclist.getEmail();
        } catch (Exception e) {
            throw new CyclistNotFoundException("Não foi possível encontrar o email do ciclista " + nameCyclist);
        }
    }

    public Email getEmailCyclist(UUID idCyclist) throws CyclistNotFoundException {
        try {
            Cyclist cyclist = cyclistRepository.getCyclist(idCyclist);

            return cyclist.getEmail();
        } catch (Exception e) {
            throw new CyclistNotFoundException("Não foi possível encontrar o email do ciclista de código " + idCyclist);
        }
    }

    public Email getEmailCyclist(Email emailCyclist) throws CyclistNotFoundException {
        try {
            Cyclist cyclist = cyclistRepository.getCyclist(emailCyclist);

            return cyclist.getEmail();
        } catch (Exception e) {
            throw new CyclistNotFoundException("Não foi possível encontrar o email do ciclista de email " + emailCyclist.getEmail());
        }
    }
}

package com.bsi.scbservicecyclistmanager.controller;

import com.bsi.scbservicecyclistmanager.controller.dto.CyclistDTO;
import com.bsi.scbservicecyclistmanager.controller.dto.EmailDTO;
import com.bsi.scbservicecyclistmanager.controller.dto.converter.CyclistConverter;
import com.bsi.scbservicecyclistmanager.controller.dto.converter.EmailConverter;
import com.bsi.scbservicecyclistmanager.domain.Email;
import com.bsi.scbservicecyclistmanager.exceptions.BadInputException;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;
import com.bsi.scbservicecyclistmanager.exceptions.UnableToDeleteException;
import com.bsi.scbservicecyclistmanager.exceptions.UnableToInsertException;
import com.bsi.scbservicecyclistmanager.service.CyclistService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.javalin.http.Context;
import io.javalin.websocket.WsHandler;

import java.io.IOException;
import java.util.UUID;

public class CyclistController {

    public static void postCyclist(Context ctx) {
        try {
            ObjectMapper mapper = new ObjectMapper();

            CyclistDTO cyclist = mapper.readValue(ctx.body(), CyclistDTO.class);
            CyclistService cyclistService = new CyclistService();

            cyclistService.postCyclist(CyclistConverter.fromDTO(cyclist));
            ctx.status(200);
        } catch (IOException | UnableToInsertException e) {
            ctx.status(500);
        }
    }

    public static void deleteCyclist(Context ctx) {

        try {
            UUID idCyclist = UUID.fromString(ctx.body());
            CyclistService cyclistService = new CyclistService();

            cyclistService.deleteCyclist(idCyclist);
            ctx.status(200);
        } catch (UnableToDeleteException e) {
            ctx.status(500);
        } catch (CyclistNotFoundException e) {
            ctx.status(404);
        }
    }

    //get que retorna as informações do ciclista buscando pelo nome, id ou email, ou sem nenhum parametro
    public static void getCyclist(Context ctx) {

        try {
            CyclistService cyclistService = new CyclistService();

            String paramName = ctx.queryParam("nome");
            String paramID = ctx.queryParam("id");
            String paramEmail = ctx.queryParam("email");

            String nameCyclist = null;
            UUID idCyclist = null;
            Email emailCyclist = null;

            if (paramName != null) {
                nameCyclist = paramName;
            } else if (paramID != null) {
                idCyclist = UUID.fromString(paramID);
            } else if (paramEmail != null) {
                EmailDTO emailDTO = new EmailDTO();
                emailDTO.setEmail(paramEmail);
                emailCyclist = EmailConverter.fromDTO(emailDTO);
            }

            ctx.json(CyclistConverter.toDTOList(cyclistService.getCyclist(nameCyclist, idCyclist, emailCyclist)));
            ctx.status(200);
        } catch (CyclistNotFoundException e) {
            ctx.status(404);
        }
    }

    public static void webSocketEvents(WsHandler wsHandler) {
    }
}

package com.bsi.scbservicecyclistmanager.controller.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class CreditCardDTO {

    private String number;
    private String owner;
    private Date validate;
    private String code;
    private String ownerDocument;

    public CreditCardDTO(){

    }
    public CreditCardDTO(
            @JsonProperty(value = "numero", required = true) String numero,
            @JsonProperty(value = "proprietario") String proprietario,
            @JsonProperty(value = "validade", required = true) @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/yyyy") Date validade,
            @JsonProperty(value = "codigo", required = true) String codigo
    ){
        this.number = numero;
        this.owner = proprietario;
        this.validate = validade;
        this.code = codigo;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Date getValidate() {
        return validate;
    }

    public void setValidate(Date validate) {
        this.validate = validate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOwnerDocument() {
        return ownerDocument;
    }

    public void setOwnerDocument(String ownerDocument) {
        this.ownerDocument = ownerDocument;
    }

}

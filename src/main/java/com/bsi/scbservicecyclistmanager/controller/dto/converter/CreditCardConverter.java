package com.bsi.scbservicecyclistmanager.controller.dto.converter;

import com.bsi.scbservicecyclistmanager.controller.dto.CreditCardDTO;
import com.bsi.scbservicecyclistmanager.domain.CreditCard;

import java.util.ArrayList;

import static java.lang.Integer.parseInt;

public class CreditCardConverter {

    public static CreditCardDTO toDTO(CreditCard creditCard){
        CreditCardDTO dto = new CreditCardDTO();

        dto.setNumber(creditCard.getNumber());
        dto.setValidate(creditCard.getValidate());
        dto.setCode(creditCard.getCode().toString());
        dto.setOwner(creditCard.getOwner());

        return dto;
    }

    public static CreditCard fromDTO(CreditCardDTO dto){
        CreditCard creditCard = new CreditCard();

        creditCard.setNumber(dto.getNumber());
        creditCard.setValidate(dto.getValidate());
        creditCard.setCode(parseInt(dto.getCode()));
        creditCard.setOwner(dto.getOwner());

        return creditCard;
    }

    public static ArrayList<CreditCardDTO> toDTOList(ArrayList<CreditCard> list){
        ArrayList<CreditCardDTO> dtoList = new ArrayList<>();

        for (CreditCard creditCard: list) {
            dtoList.add(toDTO(creditCard));
        }

        return dtoList;
    }
}

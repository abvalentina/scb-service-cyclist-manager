package com.bsi.scbservicecyclistmanager.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.awt.*;

public class DocumentDTO {
    private String type;
    private Number number;
    private Image photo;

    public DocumentDTO(){

    }

    public DocumentDTO(
            @JsonProperty(value = "tipo", required = true) String type,
            @JsonProperty(value = "numero", required = true) Number number,
            @JsonProperty(value = "foto", required = true) Image photoDocument
    ){
        this.type = type;
        this.number = number;
        this.photo = photoDocument;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Number getNumber() {
        return number;
    }

    public void setNumber(Number number) {
        this.number = number;
    }

    public Image getPhoto() {
        return photo;
    }

    public void setPhoto(Image photo) {
        this.photo = photo;
    }
}

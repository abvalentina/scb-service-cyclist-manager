package com.bsi.scbservicecyclistmanager.controller.dto;

import com.bsi.scbservicecyclistmanager.client.dto.BicycleDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class CyclistDTO {

    private UUID id;
    private EmailDTO email;
    private String name;
    private String password;
    private Boolean BRIndicator;
    private Boolean usingBike;
    private DocumentDTO document;
    private BicycleDTO bike;
    private CreditCardDTO creditCard;

        public CyclistDTO(){}

        public CyclistDTO(
                @JsonProperty(value = "id", required = true) UUID idCyclist,
                @JsonProperty(value = "email", required = true) EmailDTO email,
                @JsonProperty(value = "nome", required = true) String name,
                @JsonProperty(value = "senha", required = true) String password,
                @JsonProperty(value = "indicadorBR", required = true) Boolean BRIndicator,
                @JsonProperty(value = "usandoBicicleta") Boolean usingBike,
                @JsonProperty(value = "documento") DocumentDTO document,
                @JsonProperty(value = "bicicletaAtiva", required = true) BicycleDTO bike,
                @JsonProperty(value = "cartao", required = true) CreditCardDTO creditCard
        ){
              this.id = idCyclist;
              this.email = email;
              this.name = name;
              this.password = password;
              this.BRIndicator = BRIndicator;
              this.usingBike = usingBike;
              this.document = document;
              this.bike = bike;
              this.creditCard = creditCard;
        }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public EmailDTO getEmail() {
        return email;
    }

    public void setEmail(EmailDTO email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getBRIndicator() {
        return BRIndicator;
    }

    public void setBRIndicator(Boolean BRIndicator) {
        this.BRIndicator = BRIndicator;
    }

    public Boolean getUsingBike() {
        return usingBike;
    }

    public void setUsingBike(Boolean usingBike) {
        this.usingBike = usingBike;
    }

    public DocumentDTO getDocument() {
        return document;
    }

    public void setDocument(DocumentDTO document) {
        this.document = document;
    }

    public BicycleDTO getBike() {
        return bike;
    }

    public void setBike(BicycleDTO bike) {
        this.bike = bike;
    }

    public CreditCardDTO getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCardDTO creditCard) {
        this.creditCard = creditCard;
    }
}

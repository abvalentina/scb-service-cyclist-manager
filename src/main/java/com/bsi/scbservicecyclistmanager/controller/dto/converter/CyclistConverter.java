package com.bsi.scbservicecyclistmanager.controller.dto.converter;

import com.bsi.scbservicecyclistmanager.controller.dto.CyclistDTO;
import com.bsi.scbservicecyclistmanager.domain.Cyclist;

import java.util.*;

public class CyclistConverter {

    public static CyclistDTO toDTO(Cyclist cyclist){
        CyclistDTO dto = new CyclistDTO();

        dto.setEmail(EmailConverter.toDTO(cyclist.getEmail()));
        dto.setName(cyclist.getName());
        dto.setPassword(cyclist.getPassword());
        dto.setBRIndicator(cyclist.getBRIndicator());
        dto.setUsingBike(cyclist.getUsingBike());
        dto.setDocument(DocumentConverter.toDTO(cyclist.getDocument()));
        dto.setBike(BicycleConverter.toDTO(cyclist.getBike()));
        dto.setCreditCard(CreditCardConverter.toDTO(cyclist.getCreditCard()));

        return dto;
    }

    public static Cyclist fromDTO(CyclistDTO dto){
        Cyclist cyclist = new Cyclist();

        cyclist.setId(dto.getId());
        cyclist.setEmail(EmailConverter.fromDTO(dto.getEmail()));
        cyclist.setName(dto.getName());
        cyclist.setPassword(dto.getPassword());
        cyclist.setBRIndicator(dto.getBRIndicator());
        cyclist.setUsingBike(dto.getUsingBike());
        cyclist.setDocument(DocumentConverter.fromDTO(dto.getDocument()));
        cyclist.setBike(BicycleConverter.fromDTO(dto.getBike()));
        cyclist.setCreditCard(CreditCardConverter.fromDTO(dto.getCreditCard()));

        return cyclist;
    }

    public static ArrayList<CyclistDTO> toDTOList(ArrayList<Cyclist> list){
        ArrayList<CyclistDTO> dtoList = new ArrayList<>();

        for (Cyclist cyclist: list) {
            dtoList.add(toDTO(cyclist));
        }

        return dtoList;
    }
}

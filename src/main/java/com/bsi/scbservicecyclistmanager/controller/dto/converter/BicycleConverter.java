package com.bsi.scbservicecyclistmanager.controller.dto.converter;

import com.bsi.scbservicecyclistmanager.client.dto.BicycleDTO;
import com.bsi.scbservicecyclistmanager.domain.Bicycle;

public class BicycleConverter {

    public static BicycleDTO toDTO(Bicycle bicycle){
        BicycleDTO dto = new BicycleDTO();

        dto.setId(bicycle.getId());
        dto.setCode(bicycle.getCode());
        dto.setStatus(bicycle.getStatus());

        return dto;
    }

    public static Bicycle fromDTO(BicycleDTO dto){
        Bicycle bicycle = new Bicycle();

        bicycle.setId(dto.getId());
        bicycle.setCode(dto.getCode());
        bicycle.setStatus(dto.getStatus());

        return bicycle;
    }

}

package com.bsi.scbservicecyclistmanager.controller.dto.converter;

import com.bsi.scbservicecyclistmanager.controller.dto.DocumentDTO;
import com.bsi.scbservicecyclistmanager.domain.Document;

public class DocumentConverter {

    public static DocumentDTO toDTO(Document document){
        DocumentDTO dto = new DocumentDTO();

        dto.setType(document.getType());
        dto.setNumber(document.getNumber());
        dto.setPhoto(document.getPhoto());

        return dto;
    }

    public static Document fromDTO(DocumentDTO dto){
        Document document = new Document();

        document.setType(dto.getType());
        document.setNumber(dto.getNumber());
        document.setPhoto(dto.getPhoto());

        return document;
    }
}

package com.bsi.scbservicecyclistmanager.controller.dto.converter;

import com.bsi.scbservicecyclistmanager.controller.dto.EmailDTO;
import com.bsi.scbservicecyclistmanager.domain.Email;

import java.util.ArrayList;

public class EmailConverter {

    public static EmailDTO toDTO(Email email){
        EmailDTO dto = new EmailDTO();

        dto.setEmail(email.getEmail());

        return dto;
    }

    public static Email fromDTO(EmailDTO dto){
        Email email = new Email();

        email.setEmail(dto.getEmail());

        return email;
    }

    public static ArrayList<EmailDTO> toDTOList(ArrayList<Email> list){
        ArrayList<EmailDTO> dtoList = new ArrayList<>();

        for (Email email: list) {
            dtoList.add(toDTO(email));
        }

        return dtoList;
    }
}

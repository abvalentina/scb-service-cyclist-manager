package com.bsi.scbservicecyclistmanager.controller;

import com.bsi.scbservicecyclistmanager.controller.dto.EmailDTO;
import com.bsi.scbservicecyclistmanager.controller.dto.converter.EmailConverter;
import com.bsi.scbservicecyclistmanager.domain.Email;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;
import com.bsi.scbservicecyclistmanager.service.EmailService;
import io.javalin.http.Context;

import java.util.UUID;

public class EmailController {

    //get que retorna o email buscando pelo nome, id, email, ou sem nenhum parametro
    public static void getEmailCyclist(Context ctx) {

        try {
            EmailService emailService = new EmailService();

            String paramName = ctx.queryParam("nome");
            String paramID = ctx.queryParam("id");
            String paramEmail = ctx.queryParam("email");

            String nameCyclist = null;
            UUID idCyclist = null;
            Email emailCyclist = null;

            if (paramName != null) {
                nameCyclist = paramName;
            } else if (paramID != null) {
                idCyclist = UUID.fromString(paramID);
            } else if (paramEmail != null) {
                EmailDTO emailDTO = new EmailDTO();
                emailDTO.setEmail(paramEmail);
                emailCyclist = EmailConverter.fromDTO(emailDTO);
            }

            ctx.json(EmailConverter.toDTOList(emailService.getEmailCyclist(nameCyclist, idCyclist, emailCyclist)));
            ctx.status(200);
        } catch (CyclistNotFoundException e) {
            ctx.status(404);
        }
    }
}

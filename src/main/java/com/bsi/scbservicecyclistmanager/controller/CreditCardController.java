package com.bsi.scbservicecyclistmanager.controller;

import com.bsi.scbservicecyclistmanager.controller.dto.EmailDTO;
import com.bsi.scbservicecyclistmanager.controller.dto.converter.CreditCardConverter;
import com.bsi.scbservicecyclistmanager.controller.dto.converter.EmailConverter;
import com.bsi.scbservicecyclistmanager.domain.Email;
import com.bsi.scbservicecyclistmanager.exceptions.BadInputException;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;
import com.bsi.scbservicecyclistmanager.service.CreditCardService;
import io.javalin.http.Context;

import java.util.UUID;

public class CreditCardController {

    //get que retorna o cartão de crédito buscando pelo id, email, numero ou sem nenhum parametro
    public static void getCreditCardCyclist(Context ctx) {

        try {
            CreditCardService creditCardService = new CreditCardService();

            String paramCreditCardNumber = ctx.queryParam("nome");
            String paramID = ctx.queryParam("id");
            String paramEmail = ctx.queryParam("email");

            UUID idCyclist = null;
            String creditCardNumber = null;
            Email emailCyclist = null;

            if (paramCreditCardNumber != null) {
                creditCardNumber = paramCreditCardNumber;
            } else if (paramID != null) {
                idCyclist = UUID.fromString(paramID);
            } else if (paramEmail != null) {
                EmailDTO emailDTO = new EmailDTO();
                emailDTO.setEmail(paramEmail);
                emailCyclist = EmailConverter.fromDTO(emailDTO);
            }

            ctx.json(CreditCardConverter.toDTOList(creditCardService.getCreditCardCyclist(idCyclist, emailCyclist, creditCardNumber)));
            ctx.status(200);
        } catch (CyclistNotFoundException e) {
            ctx.status(404);
        }
    }

}

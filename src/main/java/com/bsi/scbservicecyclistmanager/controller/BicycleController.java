package com.bsi.scbservicecyclistmanager.controller;

import com.bsi.scbservicecyclistmanager.client.CreditCardClient;
import com.bsi.scbservicecyclistmanager.client.CreditCardClientImpl;
import com.bsi.scbservicecyclistmanager.client.EmailClient;
import com.bsi.scbservicecyclistmanager.client.EmailClientImpl;
import com.bsi.scbservicecyclistmanager.client.dto.converter.CreditCardConverter;
import com.bsi.scbservicecyclistmanager.domain.Cyclist;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;
import com.bsi.scbservicecyclistmanager.service.BicycleService;
import com.bsi.scbservicecyclistmanager.service.CyclistService;
import io.javalin.http.Context;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Objects;
import java.util.UUID;

import static java.lang.Integer.parseInt;

public class BicycleController {

    public static void rentBike(Context ctx) {
        BicycleService bicycleService = new BicycleService();
        CyclistService cyclistService = new CyclistService();
        CreditCardClient creditCardClient = new CreditCardClientImpl();
        EmailClient emailClient = new EmailClientImpl();

        try {
            UUID idCyclist = UUID.fromString(Objects.requireNonNull(ctx.queryParam("idCyclist")));
            String idBike = Objects.requireNonNull(ctx.queryParam("idBike"));


            boolean wasRented = bicycleService.rentBike(idCyclist, parseInt(idBike));
            Cyclist cyclist = cyclistService.getCyclist(null, idCyclist, null).get(0);
            Boolean charged = creditCardClient.chargeCreditCard(CreditCardConverter.toDTO(cyclist.getCreditCard()));
            String description = "Sua corrida com a Bike", message = "o valor da sua corrida foi de :" + cyclist.getCreditCard().getChargeAmount();

            if(wasRented && charged){
                emailClient.sendEmail(cyclist.getEmail(), description, message);
                ctx.status(201);
            }
        } catch (CyclistNotFoundException e) {
            ctx.status(404);
        }catch (URISyntaxException | InterruptedException | IOException e){
            ctx.status(500);
        }

    }
}

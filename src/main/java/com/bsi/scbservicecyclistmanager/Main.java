package com.bsi.scbservicecyclistmanager;

import com.bsi.scbservicecyclistmanager.controller.BicycleController;
import com.bsi.scbservicecyclistmanager.controller.CreditCardController;
import com.bsi.scbservicecyclistmanager.controller.CyclistController;
import com.bsi.scbservicecyclistmanager.controller.EmailController;
import com.bsi.scbservicecyclistmanager.util.ErrorResponse;
import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;
import org.jetbrains.annotations.NotNull;

import static io.javalin.apibuilder.ApiBuilder.*;


public class Main {

    public static void main(String[] args) {

        Javalin.create(config -> {
            config.defaultContentType = "application/json";
            config.autogenerateEtags = true;
            config.asyncRequestTimeout = 10_000L;
            config.enforceSsl = true;
        }).get("/", ctx -> ctx.result("Hello Heroku"))
            .routes(() -> {
                path("rent-bike", () -> post(BicycleController::rentBike));
                path("cyclist", () -> {
                    path(":idCyclist", () -> delete(CyclistController::deleteCyclist));
                    post(CyclistController::postCyclist);
                    get(CyclistController::getCyclist);
                });
                path("email", () -> get(EmailController::getEmailCyclist));
                path("credit-card", () -> get(CreditCardController::getCreditCardCyclist));
            }).start(getHerokuAssignedPort());

        getConfiguredOpenApiPlugin();
    }

    private static int getHerokuAssignedPort() {
        String herokuPort = System.getenv("PORT");
        if (herokuPort != null) {
            return Integer.parseInt(herokuPort);
        }
        return 7000;
    }

    @NotNull
    private static OpenApiPlugin getConfiguredOpenApiPlugin() {
        Info info = new Info().version("1.0").title("User API").description("Demo API with 5 operations");
        OpenApiOptions options = new OpenApiOptions(info).activateAnnotationScanningFor("io.javalin.example.java")
                .path("/swagger-docs") // endpoint for OpenAPI json
                .swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
                .reDoc(new ReDocOptions("/redoc")) // endpoint for redoc
                .defaultDocumentation(doc -> {
                    doc.json("500", ErrorResponse.class);
                    doc.json("503", ErrorResponse.class);

                });
        return new OpenApiPlugin(options);
    }
}

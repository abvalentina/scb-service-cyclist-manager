package com.bsi.scbservicecyclistmanager.exceptions;

public class UnableToDeleteException extends Exception{

    public UnableToDeleteException(String message){
        super(message);
    }
}

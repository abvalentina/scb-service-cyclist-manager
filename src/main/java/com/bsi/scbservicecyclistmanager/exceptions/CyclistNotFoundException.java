package com.bsi.scbservicecyclistmanager.exceptions;

public class CyclistNotFoundException extends Exception{

    public CyclistNotFoundException(String message){
        super(message);
    }
}

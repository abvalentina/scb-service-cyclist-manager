package com.bsi.scbservicecyclistmanager.exceptions;

public class UnableToInsertException extends Exception{

    public UnableToInsertException(String message){
        super(message);
    }
}

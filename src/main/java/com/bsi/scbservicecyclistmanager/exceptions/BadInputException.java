package com.bsi.scbservicecyclistmanager.exceptions;

public class BadInputException extends Exception{
    public BadInputException(String message){ super(message);}
}

package com.bsi.scbservicecyclistmanager.service;

import com.bsi.scbservicecyclistmanager.client.BraidClientImpl;
import com.bsi.scbservicecyclistmanager.domain.Lock;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class LockService {
    BraidClientImpl braidClient;

    public List<Lock> getLocks(Integer id, Integer number) throws InterruptedException, IOException, URISyntaxException {
        List<Lock> braid;
        if(id != null)
            braid = braidClient.getBraids(id, null);
        else if(number != null)
            braid = braidClient.getBraids(null, number);
        else
            braid = braidClient.getBraids(null, null);

        return braid;
    }
}

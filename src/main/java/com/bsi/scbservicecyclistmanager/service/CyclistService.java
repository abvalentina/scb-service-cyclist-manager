package com.bsi.scbservicecyclistmanager.service;

import com.bsi.scbservicecyclistmanager.domain.Cyclist;
import com.bsi.scbservicecyclistmanager.domain.Email;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;
import com.bsi.scbservicecyclistmanager.exceptions.UnableToDeleteException;
import com.bsi.scbservicecyclistmanager.exceptions.UnableToInsertException;
import com.bsi.scbservicecyclistmanager.repository.CyclistRepository;

import java.util.ArrayList;
import java.util.UUID;

public class CyclistService {

    CyclistRepository cyclistRepository = CyclistRepository.instance();



    public void postCyclist(Cyclist cyclist) throws UnableToInsertException {
         cyclistRepository.postCyclist(cyclist);
    }

    public void deleteCyclist(UUID idCyclist) throws UnableToDeleteException, CyclistNotFoundException {
        cyclistRepository.deleteCyclist(idCyclist);
    }


    public ArrayList<Cyclist> getCyclist(String nameCyclist, UUID idCyclist, Email emailCyclist) throws CyclistNotFoundException{
        ArrayList<Cyclist> cyclists = new ArrayList<>();

        if(nameCyclist != null)
            cyclists.add(cyclistRepository.getCyclist(nameCyclist));
        else if(idCyclist != null)
            cyclists.add(cyclistRepository.getCyclist(idCyclist));
        else if(emailCyclist != null)
            cyclists.add(cyclistRepository.getCyclist(emailCyclist));
        else
            cyclists = cyclistRepository.getCyclist();

        return cyclists;
    }
}

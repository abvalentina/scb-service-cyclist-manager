package com.bsi.scbservicecyclistmanager.service;

import com.bsi.scbservicecyclistmanager.domain.CreditCard;
import com.bsi.scbservicecyclistmanager.domain.Email;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;
import com.bsi.scbservicecyclistmanager.repository.CreditCardRepository;

import java.util.ArrayList;
import java.util.UUID;

public class CreditCardService {


    CreditCardRepository creditCardRepository = CreditCardRepository.instance();

    public ArrayList<CreditCard> getCreditCardCyclist(UUID idCyclist, Email emailCyclist, String creditCardNumber) throws CyclistNotFoundException {
        ArrayList<CreditCard> creditCardList = new ArrayList<>();

        if (idCyclist != null)
            creditCardList.add(creditCardRepository.getCreditCardCyclist(idCyclist));
        else if (emailCyclist != null)
            creditCardList.add(creditCardRepository.getCreditCardCyclist(emailCyclist));
        else if (creditCardNumber != null)
            creditCardList.add(creditCardRepository.getCreditCardCyclist(creditCardNumber));
        else
            creditCardList = creditCardRepository.getCreditCardCyclist();

        return creditCardList;
    }
}

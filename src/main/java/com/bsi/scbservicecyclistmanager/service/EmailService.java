package com.bsi.scbservicecyclistmanager.service;

import com.bsi.scbservicecyclistmanager.domain.Email;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;
import com.bsi.scbservicecyclistmanager.repository.EmailRepository;

import java.util.ArrayList;
import java.util.UUID;

public class EmailService {

    private EmailRepository emailRepository = EmailRepository.instance();

    public ArrayList<Email> getEmailCyclist(String nameCyclist, UUID idCyclist, Email emailCyclist) throws CyclistNotFoundException {
        ArrayList<Email> emailList = new ArrayList<>();

        if (nameCyclist != null)
            emailList.add(emailRepository.getEmailCyclist(nameCyclist));
        else if (idCyclist != null)
            emailList.add(emailRepository.getEmailCyclist(idCyclist));
        else if (emailCyclist != null)
            emailList.add(emailRepository.getEmailCyclist(emailCyclist));
        else
            emailList = emailRepository.getEmailCyclist();

        return emailList;
    }

}

package com.bsi.scbservicecyclistmanager.service;

import com.bsi.scbservicecyclistmanager.client.BicycleClient;
import com.bsi.scbservicecyclistmanager.client.BicycleClientImpl;
import com.bsi.scbservicecyclistmanager.domain.Bicycle;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;
import com.bsi.scbservicecyclistmanager.repository.BicycleRepository;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

public class BicycleService {

    BicycleRepository bicycleRepository = new BicycleRepository();
    BicycleClient bicycleClient = new BicycleClientImpl();

    public boolean rentBike(UUID idCyclist, Integer idBike) throws CyclistNotFoundException, URISyntaxException, IOException, InterruptedException {
        Bicycle firstFreeBike = bicycleClient.getBikes(null, idBike, null).get(0);

        if (firstFreeBike.getStatus().equals("DISPONÍVEL")) {

            firstFreeBike.setStatus("INDISPONÍVEL");
            bicycleClient.updateBikeStatus(firstFreeBike);
        }

        return bicycleRepository.rentBike(idCyclist);
    }
}

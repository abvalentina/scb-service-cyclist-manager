package com.bsi.scbservicecyclistmanager.client;

import com.bsi.scbservicecyclistmanager.domain.Email;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.net.URISyntaxException;

public interface EmailClient {

    void sendEmail(Email email, String subject, String description) throws URISyntaxException, IOException, InterruptedException;
}

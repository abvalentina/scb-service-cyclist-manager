package com.bsi.scbservicecyclistmanager.client;

import com.bsi.scbservicecyclistmanager.domain.Bicycle;
import io.javalin.http.Context;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public interface BicycleClient {

    void updateBikeStatus(Bicycle bike) throws URISyntaxException, IOException, InterruptedException;

    List<Bicycle> getBikes(String idCyclist, Integer idBike, Integer idLock) throws URISyntaxException, IOException, InterruptedException;
}

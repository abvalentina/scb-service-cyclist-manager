package com.bsi.scbservicecyclistmanager.client.dto.converter;

import com.bsi.scbservicecyclistmanager.client.dto.BicycleDTO;
import com.bsi.scbservicecyclistmanager.domain.Bicycle;
import org.jetbrains.annotations.NotNull;

public class BicycleConverter {

    @NotNull
    public static BicycleDTO toDTO(@NotNull Bicycle bicycle){
        BicycleDTO dto = new BicycleDTO();

        dto.setId(bicycle.getId());
        dto.setCode(bicycle.getCode());
        dto.setStatus(bicycle.getStatus());
        dto.setMarca(bicycle.getBrand());
        dto.setModelo(bicycle.getModel());
        dto.setAno(bicycle.getYear());
        dto.setLocalizacao(bicycle.getLocalization());

        return dto;
    }

    @NotNull
    public static Bicycle fromDTO(@NotNull BicycleDTO dto){
        Bicycle bicycle = new Bicycle();

        bicycle.setId(dto.getId());
        bicycle.setCode(dto.getCode());
        bicycle.setStatus(dto.getStatus());
        bicycle.setBrand(dto.getMarca());
        bicycle.setModel(dto.getModelo());
        bicycle.setYear(dto.getAno());
        bicycle.setLocalization(dto.getLocalizacao());

        return bicycle;
    }

}

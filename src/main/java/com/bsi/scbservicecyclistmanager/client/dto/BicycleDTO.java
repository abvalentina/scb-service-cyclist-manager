package com.bsi.scbservicecyclistmanager.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class BicycleDTO {

    private Integer id;
    private Integer code;
    private String status;
    private String marca;
    private String modelo;
    private Integer ano;
    private String localizacao;

    public BicycleDTO(
            @JsonProperty(value = "id", required = true) Integer idBicycle,
            @JsonProperty(value = "code", required = true) Integer code,
            @JsonProperty(value = "status", required = true) String status
    ){
        this.id = idBicycle;
        this.code = code;
        this.status = status;
    }

    public BicycleDTO() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }


}

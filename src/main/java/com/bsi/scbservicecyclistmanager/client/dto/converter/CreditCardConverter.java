package com.bsi.scbservicecyclistmanager.client.dto.converter;


import com.bsi.scbservicecyclistmanager.client.dto.CreditCardDTO;
import com.bsi.scbservicecyclistmanager.domain.CreditCard;

import java.io.IOException;
import java.util.ArrayList;

import static java.lang.Integer.parseInt;

public class CreditCardConverter {

    public static CreditCardDTO toDTO(CreditCard creditCard){
        CreditCardDTO dto = new CreditCardDTO();

        dto.setNumero(creditCard.getNumber());
        dto.setProprietario(creditCard.getOwner());
        dto.setValidade(creditCard.getValidate());
        dto.setCodigo(creditCard.getCode().toString());
        dto.setCpfProprietario(creditCard.getOwnerDocument());
        if (creditCard.getBillingAddress() != null)
            dto.setEnderecoCobranca(BillingAddressConverter.toDTO(creditCard.getBillingAddress()));
        dto.setValorCobranca(creditCard.getChargeAmount());

        return dto;
    }

    public static CreditCard fromDTO(CreditCardDTO dto) throws IOException {
        CreditCard creditCard = new CreditCard();

        creditCard.setNumber(dto.getNumero());
        creditCard.setOwner(dto.getProprietario());
        creditCard.setValidate(dto.getValidade());
        creditCard.setCode(parseInt(dto.getCodigo()));
        creditCard.setOwnerDocument(dto.getCpfProprietario());
        if (dto.getEnderecoCobranca() != null)
            creditCard.setBillingAddress(BillingAddressConverter.fromDTO(dto.getEnderecoCobranca()));
        creditCard.setChargeAmount(dto.getValorCobranca());

        return creditCard;
    }

    public static ArrayList<CreditCardDTO> toDTOList(ArrayList<CreditCard> list){
        ArrayList<CreditCardDTO> dtoList = new ArrayList<>();

        for (CreditCard creditCard: list) {
            dtoList.add(toDTO(creditCard));
        }

        return dtoList;
    }
}

package com.bsi.scbservicecyclistmanager.client.dto.converter;

import com.bsi.scbservicecyclistmanager.client.dto.BillingAddressDTO;
import com.bsi.scbservicecyclistmanager.domain.BillingAddress;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class BillingAddressConverter {

    @NotNull
    public static BillingAddressDTO toDTO(@NotNull BillingAddress billingAddress){
        BillingAddressDTO dto = new BillingAddressDTO();

        dto.setRua(billingAddress.getStreet());
        dto.setNumero(billingAddress.getNumber());
        dto.setComplemento(billingAddress.getComplement());
        dto.setBairro(billingAddress.getNeighborhood());
        dto.setCidade(billingAddress.getCity());
        dto.setEstado(billingAddress.getState());
        dto.setCep(billingAddress.getCep());

        return dto;
    }
    
    @NotNull
    public static BillingAddress fromDTO(BillingAddressDTO dto) throws IOException {
        BillingAddress billingAddress = new BillingAddress();
        
        billingAddress.setStreet(dto.getRua());
        billingAddress.setNumber(dto.getNumero());
        billingAddress.setComplement(dto.getComplemento());
        billingAddress.setNeighborhood(dto.getBairro());
        billingAddress.setCity(dto.getCidade());
        billingAddress.setState(dto.getEstado());
        billingAddress.setCep(dto.getCep());

        return billingAddress;
    }
}

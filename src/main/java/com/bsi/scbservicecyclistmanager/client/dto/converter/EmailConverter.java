package com.bsi.scbservicecyclistmanager.client.dto.converter;

import com.bsi.scbservicecyclistmanager.client.dto.EmailDTO;
import com.bsi.scbservicecyclistmanager.domain.Email;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class EmailConverter {

    @NotNull
    public static EmailDTO toDTO(@NotNull Email email){
        EmailDTO dto = new EmailDTO();

        dto.setEmail(email.getEmail());

        return dto;
    }

    @NotNull
    public static Email fromDTO(@NotNull EmailDTO dto){
        Email email = new Email();

        email.setEmail(dto.getEmail());

        return email;
    }

    @NotNull
    public static ArrayList<EmailDTO> toDTOList(@NotNull ArrayList<Email> list){
        ArrayList<EmailDTO> dtoList = new ArrayList<>();

        for (Email email: list) {
            dtoList.add(toDTO(email));
        }

        return dtoList;
    }
}

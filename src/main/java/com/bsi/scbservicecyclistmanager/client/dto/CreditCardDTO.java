package com.bsi.scbservicecyclistmanager.client.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class CreditCardDTO {

    private String numero;
    private String proprietario;
    private Date validade;
    private String codigo;
    private String cpfProprietario;
    private BillingAddressDTO enderecoCobranca;
    private float valorCobranca;

    public CreditCardDTO(){

    }
    public CreditCardDTO(
            @JsonProperty(value = "numero", required = true) String numero,
            @JsonProperty(value = "proprietario") String proprietario,
            @JsonProperty(value = "validade", required = true) @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/yyyy") Date validade,
            @JsonProperty(value = "codigo", required = true) String codigo
    ){
        this.numero = numero;
        this.proprietario = proprietario;
        this.validade = validade;
        this.codigo = codigo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getProprietario() {
        return proprietario;
    }

    public void setProprietario(String proprietario) {
        this.proprietario = proprietario;
    }

    public Date getValidade() {
        return validade;
    }

    public void setValidade(Date validade) {
        this.validade = validade;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCpfProprietario() {
        return cpfProprietario;
    }

    public void setCpfProprietario(String cpfProprietario) {
        this.cpfProprietario = cpfProprietario;
    }

    public BillingAddressDTO getEnderecoCobranca() {
        return enderecoCobranca;
    }

    public void setEnderecoCobranca(BillingAddressDTO enderecoCobranca) {
        this.enderecoCobranca = enderecoCobranca;
    }

    public float getValorCobranca() {
        return valorCobranca;
    }

    public void setValorCobranca(float valorCobranca) {
        this.valorCobranca = valorCobranca;
    }
}

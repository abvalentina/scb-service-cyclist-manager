package com.bsi.scbservicecyclistmanager.client;

import com.bsi.scbservicecyclistmanager.domain.Bicycle;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

public class BicycleClientImpl implements BicycleClient {

    @Override
    public void updateBikeStatus(Bicycle bike) throws URISyntaxException, IOException, InterruptedException {
        ObjectMapper mapper = new ObjectMapper();
        HttpRequest request = HttpRequest
                .newBuilder(new URI("https://bicicletario.herokuapp.com/bicicleta"))
                .POST(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(bike)))
                .build();

        HttpClient.newBuilder()
                .build()
                .send(request, HttpResponse.BodyHandlers.ofString());
    }

    @Override
    public List<Bicycle> getBikes(String idCyclist, Integer idBike, Integer idLock) throws URISyntaxException, IOException, InterruptedException {
        ObjectMapper mapper = new ObjectMapper();
        HttpRequest request = HttpRequest
                .newBuilder(new URI("https://bicicletario.herokuapp.com/bicicleta"))
                .GET()
                .build();

        HttpResponse response = HttpClient.newBuilder()
                .build()
                .send(request, HttpResponse.BodyHandlers.ofString());

        return mapper.readValue(response.body().toString(), new TypeReference<List<Bicycle>>() { });
    }
}

package com.bsi.scbservicecyclistmanager.client;

import com.bsi.scbservicecyclistmanager.client.dto.CreditCardDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class CreditCardClientImpl implements CreditCardClient {

    @Override
    public Boolean chargeCreditCard(CreditCardDTO creditCard) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            HttpRequest request = HttpRequest
                    .newBuilder(new URI("https://scbintegracoes.herokuapp.com/cobraCartao"))
                    .POST(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(creditCard)))
                    .build();

            HttpClient.newBuilder()
                    .build()
                    .send(request, HttpResponse.BodyHandlers.ofString());
            return true;
        } catch (URISyntaxException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }
}

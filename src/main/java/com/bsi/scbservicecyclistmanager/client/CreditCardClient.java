package com.bsi.scbservicecyclistmanager.client;

import com.bsi.scbservicecyclistmanager.client.dto.CreditCardDTO;

import java.io.IOException;
import java.net.URISyntaxException;

public interface CreditCardClient {

    Boolean chargeCreditCard(CreditCardDTO charge) throws URISyntaxException, IOException, InterruptedException;
}

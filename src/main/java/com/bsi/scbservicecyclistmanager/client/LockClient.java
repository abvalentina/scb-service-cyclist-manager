package com.bsi.scbservicecyclistmanager.client;

import com.bsi.scbservicecyclistmanager.domain.Lock;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public interface LockClient {

    List<Lock> getBraids(Integer id, Integer number) throws URISyntaxException, IOException, InterruptedException;
}

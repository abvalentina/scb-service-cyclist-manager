package com.bsi.scbservicecyclistmanager.client;

import com.bsi.scbservicecyclistmanager.domain.Email;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class EmailClientImpl implements EmailClient{

    @Override
    public void sendEmail(Email email, String subject, String description) throws URISyntaxException, IOException, InterruptedException {
        ObjectMapper mapper = new ObjectMapper();
        HttpRequest request = HttpRequest
                .newBuilder(new URI("https://scbintegracoes.herokuapp.com/enviaEmail"))
                .POST(HttpRequest.BodyPublishers.ofString(mapper.writeValueAsString(email)))
                .build();

        HttpClient.newBuilder()
                .build()
                .send(request, HttpResponse.BodyHandlers.ofString());
    }

}

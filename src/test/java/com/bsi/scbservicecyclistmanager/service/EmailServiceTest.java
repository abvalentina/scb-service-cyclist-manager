package com.bsi.scbservicecyclistmanager.service;

import com.bsi.scbservicecyclistmanager.domain.Bicycle;
import com.bsi.scbservicecyclistmanager.domain.Cyclist;
import com.bsi.scbservicecyclistmanager.domain.Email;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;
import com.bsi.scbservicecyclistmanager.repository.EmailRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmailServiceTest {

    @Mock
    private EmailRepository emailRepository;

    @InjectMocks
    private EmailService emailService = new EmailService();

    public String nameCyclist;
    public UUID idCyclist;
    public Email emailCyclist;

    public static ArrayList<Email> emailsList;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        nameCyclist = "Lawrence";
        idCyclist = UUID.fromString("f4e4d604-deef-4aae-a907-a68ece8a607a");
        emailCyclist = new Email("lawrence@gmail.com");
        emailsList = new ArrayList<>();
        emailsList.add(new Email("davi@gmail.com"));
    }

    @Test
    public void getEmailCyclistWhenNamePassed() throws CyclistNotFoundException {
        when(emailRepository.getEmailCyclist(any(String.class))).thenReturn(emailCyclist);

        Assert.assertNotNull(emailService.getEmailCyclist(nameCyclist, null, null));
    }

    @Test
    public void getEmailCyclistWhenIDPassed() throws CyclistNotFoundException {
        when(emailRepository.getEmailCyclist(any(UUID.class))).thenReturn(emailCyclist);

        Assert.assertNotNull(emailService.getEmailCyclist(null, idCyclist, null));
    }

    @Test
    public void getEmailCyclistWhenEmailPassed() throws CyclistNotFoundException {
        when(emailRepository.getEmailCyclist(any(Email.class))).thenReturn(emailCyclist);

        Assert.assertNotNull(emailService.getEmailCyclist(null, null, emailCyclist));
    }

    @Test
    public void getEmailsCyclistsWhenNothingPassed() throws CyclistNotFoundException {
        when(emailRepository.getEmailCyclist()).thenReturn(emailsList);

        Assert.assertNotNull(emailService.getEmailCyclist(null, null, null));
    }

    @Test(expected = CyclistNotFoundException.class)
    public void cyclistNotFoundExceptionShouldThrow() throws CyclistNotFoundException {
        when(emailRepository.getEmailCyclist(any(Email.class))).thenThrow(new CyclistNotFoundException(""));

        emailService.getEmailCyclist(null, null, emailCyclist);
    }
}

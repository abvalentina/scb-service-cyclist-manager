package com.bsi.scbservicecyclistmanager.service;

import com.bsi.scbservicecyclistmanager.domain.CreditCard;
import com.bsi.scbservicecyclistmanager.domain.Email;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;
import com.bsi.scbservicecyclistmanager.repository.CreditCardRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class CreditCardServiceTest {

    @Mock
    CreditCardRepository creditCardRepository;

    @InjectMocks
    CreditCardService creditCardService = new CreditCardService();

    public UUID idCyclist;
    public Email emailCyclist;
    public String creditCardNumber;
    public CreditCard creditCardCyclist;

    public static ArrayList<CreditCard> creditCardsList;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        idCyclist = UUID.fromString("f4e4d604-deef-4aae-a907-a68ece8a607a");
        emailCyclist = new Email("lawrence@gmail.com");
        creditCardNumber = "1236654";
        creditCardCyclist = new CreditCard();
        creditCardsList = new ArrayList<>();
        creditCardsList.add(new CreditCard());
    }

    @Test
    public void getCreditCardWhenEmailPassed() throws CyclistNotFoundException {
        when(creditCardRepository.getCreditCardCyclist(any(Email.class))).thenReturn(creditCardCyclist);

        Assert.assertNotNull(creditCardService.getCreditCardCyclist(null, emailCyclist, null));
    }

    @Test
    public void getCreditCardWhenIDPassed() throws CyclistNotFoundException {
        when(creditCardRepository.getCreditCardCyclist(any(UUID.class))).thenReturn(creditCardCyclist);

        Assert.assertNotNull(creditCardService.getCreditCardCyclist(idCyclist, null, null));
    }

    @Test
    public void getCreditCardWhenCreditCardNumberPassed() throws CyclistNotFoundException {
        when(creditCardRepository.getCreditCardCyclist(any(String.class))).thenReturn(creditCardCyclist);

        Assert.assertNotNull(creditCardService.getCreditCardCyclist(null, null, creditCardNumber));
    }

    @Test
    public void getCreditCardWhenNothingPassed() throws CyclistNotFoundException {
        when(creditCardRepository.getCreditCardCyclist()).thenReturn(creditCardsList);

        Assert.assertNotNull(creditCardService.getCreditCardCyclist(null, null, null));
    }


    @Test(expected = CyclistNotFoundException.class)
    public void shouldThrowCyclistNotFoundException() throws CyclistNotFoundException{
        when(creditCardRepository.getCreditCardCyclist(any(Email.class))).thenThrow(new CyclistNotFoundException(""));

        creditCardService.getCreditCardCyclist(null, emailCyclist, null);
    }
}

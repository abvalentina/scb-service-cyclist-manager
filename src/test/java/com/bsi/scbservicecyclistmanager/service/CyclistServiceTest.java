package com.bsi.scbservicecyclistmanager.service;

import com.bsi.scbservicecyclistmanager.domain.Cyclist;
import com.bsi.scbservicecyclistmanager.domain.Email;
import com.bsi.scbservicecyclistmanager.exceptions.CyclistNotFoundException;
import com.bsi.scbservicecyclistmanager.exceptions.UnableToDeleteException;
import com.bsi.scbservicecyclistmanager.exceptions.UnableToInsertException;
import com.bsi.scbservicecyclistmanager.repository.CyclistRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class CyclistServiceTest {

    @Mock
    CyclistRepository cyclistRepository;

    @InjectMocks
    CyclistService cyclistService = new CyclistService();

    public String nameCyclist;
    public UUID idCyclist;
    public Email emailCyclist;
    public Cyclist cyclist;

    public static ArrayList<Cyclist> cyclistsList;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        nameCyclist = "Lawrence";
        idCyclist = UUID.fromString("f4e4d604-deef-4aae-a907-a68ece8a607a");
        emailCyclist = new Email("lawrence@gmail.com");
        cyclist = new Cyclist();
        cyclistsList = new ArrayList<>();
        cyclistsList.add(new Cyclist());
    }

    @Test
    public void getCyclistWhenNamePassed() throws CyclistNotFoundException {
        when(cyclistRepository.getCyclist(any(String.class))).thenReturn(cyclist);

        Assert.assertNotNull(cyclistService.getCyclist(nameCyclist, null, null));
    }

    @Test
    public void getCyclistWhenIDPassed() throws CyclistNotFoundException {
        when(cyclistRepository.getCyclist(any(UUID.class))).thenReturn(cyclist);

        Assert.assertNotNull(cyclistService.getCyclist(null, idCyclist, null));
    }

    @Test
    public void getCyclistWhenEmailPassed() throws CyclistNotFoundException {
        when(cyclistRepository.getCyclist(any(Email.class))).thenReturn(cyclist);

        Assert.assertNotNull(cyclistService.getCyclist(null, null, emailCyclist));
    }

    @Test
    public void getCyclistsWhenNothingPassed() throws CyclistNotFoundException {
        when(cyclistRepository.getCyclist()).thenReturn(cyclistsList);

        Assert.assertNotNull(cyclistService.getCyclist(null, null, null));
    }

    @Test(expected = CyclistNotFoundException.class)
    public void cyclistNotFoundExceptionShouldThrow() throws CyclistNotFoundException {
        when(cyclistRepository.getCyclist(any(Email.class))).thenThrow(new CyclistNotFoundException(""));

        cyclistService.getCyclist(null, null, emailCyclist);
    }

    @Test(expected = UnableToInsertException.class)
    public void shouldPostThrowUnableToInsert() throws UnableToInsertException {
        doThrow(new UnableToInsertException("")).when(cyclistRepository).postCyclist(any(Cyclist.class));

        cyclistService.postCyclist(cyclist);
    }

    @Test(expected = UnableToDeleteException.class)
    public void shouldPostThrowUnableToDelete() throws UnableToDeleteException, CyclistNotFoundException {
        doThrow(new UnableToDeleteException("")).when(cyclistRepository).deleteCyclist(any(UUID.class));

        cyclistService.deleteCyclist(idCyclist);
    }

}
